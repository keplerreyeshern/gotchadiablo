<?php

namespace App\Http\Controllers;

use App\Mail\MessagesRecieved;
use App\Mail\Reservations;
use App\Package;
use App\Image;
use App\Reservation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $active = 'main';
        $packages = Package::orderBy('id', 'ASC')->limit(3)->get();
        return view('main.main', compact('active', 'packages'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contact()
    {
        $active = 'contact';
        return view('main.contact', compact('active'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function gallery()
    {
        $active = 'gallery';
        $images = Image::get();
        return view('main.gallery', compact('active', 'images'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function packages()
    {
        $active = 'packages';
        $packages = Package::get();
        return view('main.packages', compact('active', 'packages'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reservations()
    {
        $active = 'reservations';
        $reservations = Reservation::where('user_id', auth()->user()->id)->get();
        return view('main.reservations', compact('active', 'reservations'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showPackage($id)
    {
        $active = 'packages';
        $package = Package::findOrFail($id);
        return view('main.package', compact('active', 'package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createReservation()
    {
        $active = 'reservations';
        $packages = Package::get();
        return view('main.createR', compact('active','packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeReservation(Request $request)
    {
        $data = $request->validate([
            'package' => 'required',
            'date' => 'required',
        ], [
            'package.required' => 'El campo paquete es obligatorio',
            'date.required' => 'El campo fecha es obligatorio',
        ]);

        Mail::to('torosoprano@hotmail.com')
            ->queue(new Reservations($data));

//        return new Reservations($data);


        Reservation::create([
            'user_id' => auth()->user()->id,
            'package_id' => $data['package'],
            'date' => $data['date'],
            'status' => 1
        ]);


        return redirect('reservaciones')->with('success', 'La reservación se creo correctamente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeContact(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required|string|min:10|max:10',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ], [
            'name.required' => 'El campo nombre es obligatorio',
            'email.required' => 'El campo email es obligatorio',
            'phone.required' => 'El campo telefono es obligatorio',
            'phone.min' => 'El campo telefono debe tener 10 digitos',
            'phone.max' => 'El campo telefono debe tener 10 digitos',
            'message.required' => 'El campo mensaje es obligatorio',
            'g-recaptcha-response.required' => 'El captcha es obligatorio'
        ]);

        Mail::to('administrador@gotchadiablo.com')
            ->queue(new MessagesRecieved($data));

//        return new MessagesRecieved($data);

        return redirect('contacto')->with('success', 'El mensaje se envio correctamente');
    }

}
