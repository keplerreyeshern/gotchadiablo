<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades;

class ImagesController extends Controller
{
    protected $active;

    public function __construct()
    {
        $this->active = 'images';
        View::share('active', $this->active);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::get();
        return view('gallery.list', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|unique:packages|string|min:4',
            'description' => 'required',
            'image' => 'required'
        ], [
            'title.required' => 'El campo titulo es obligatorio',
            'title.unique' => 'El titulo para la imagen ya se encuentra registrado',
            'title.min' => 'El campo titulo debe tener como minimo 4 caracteres',
            'description.required' => 'El campo descripción es obligatorio',
            'image.required' => 'El campo imagen es obligatorio',
        ]);
        if ($request->file('image')){
            $path = Facades\Storage::disk('public')->put('images', $request->file('image'));
            $image = $path;
        }
        Image::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'image' => $image,
        ]);
        return redirect('admin/imagenes')->with('success', 'La Imagen se creo correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = Image::findOrFail($id);
        return view('gallery.view', compact('image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Image::findOrFail($id);
        return view('gallery.edit', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = Image::findOrFail($id);
        $data = $request->validate([
            'title' => 'required|unique:packages,id|string|min:4',
            'description' => 'required',
        ], [
            'title.required' => 'El campo titulo es obligatorio',
            'title.unique' => 'El titulo para el paquete ya se encuentra registrado',
            'title.min' => 'El campo titulo debe tener como minimo 4 caracteres',
            'description.required' => 'El campo descripción es obligatorio',
        ]);
        if ($request->file('image')){
            $path = Facades\Storage::disk('public')->put('images', $request->file('image'));
            $image->image = $path;
        }

        $image->title = $data['title'];
        $image->description = $data['description'];
        $image->update();

        return redirect('admin/imagenes')->with('success', 'La imagen se actualizo correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Image::findOrFail($id);
        $image->delete();
        return redirect('admin/imagenes')->with('success', 'La Imagen se elimino correctamente');
    }
}
