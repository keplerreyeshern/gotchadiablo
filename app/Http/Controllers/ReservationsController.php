<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ReservationsController extends Controller
{
    protected $active;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->active = 'resevations';
        View::share('active', $this->active);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resevationsLast = Reservation::whereDate('date', '<=', date('Y-m-d'))->get();
        foreach ($resevationsLast as $reservation){
            if ($reservation->status == 1){
                $reservation->status = 5;
            } elseif ($reservation->status == 5){
                $reservation->status = 5;
            } elseif ($reservation->status == 4){
                $reservation->status = 4;
            } else {
                $reservation->status = 4;
            }
            $reservation->update();
        }
        $reservations = Reservation::get();
        return view('reservations.list', compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservation = Reservation::findOrFail($id);
        return view('reservations.view', compact('reservation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resevation = Reservation::findOrFail($id);
        $resevation->delete();
        return redirect('reservaciones')->with('success', 'La reservación se elimino correctamente');
    }
}
