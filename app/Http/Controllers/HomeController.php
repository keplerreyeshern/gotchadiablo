<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = $request->validate([
            'name' => 'required|string|min:8',
            'email' => 'required|unique:user,id',
        ], [
            'name.required' => 'El campo titulo es obligatorio',
            'email.unique' => 'El titulo para el paquete ya se encuentra registrado',
            'name.min' => 'El campo titulo debe tener como minimo 8 caracteres',
            'email.required' => 'El campo email es obligatorio'
        ]);


        $user->title = $data['name'];
        $user->description = $data['email'];
        $user->update();

        return redirect('home')->with('success', 'El paquete se actualizo correctamente');
    }
}
