<?php

namespace App\Http\Controllers;

use App\Image;
use App\Package;
use App\Reservation;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $active = 'dashboard';
        $countPackages = count(Package::get());
        $countImages = count(Image::get());
        $countReservations = count(Reservation::get());
        $countUsers = count(User::get());
        return view('dashboard.index', compact('active', 'countPackages', 'countImages', 'countReservations', 'countUsers'));
    }
}
