<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades;

class PackagesController extends Controller
{
    protected $active;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->active = 'packages';
        View::share('active', $this->active);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::get();
        return view('packages.list', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|unique:packages|string|min:4',
            'description' => 'required',
            'price' => 'required',
            'brief_description' => 'required',
        ], [
            'title.required' => 'El campo titulo es obligatorio',
            'title.unique' => 'El titulo para el paquete ya se encuentra registrado',
            'title.min' => 'El campo titulo debe tener como minimo 4 caracteres',
            'brief_description.required' => 'El campo codigo es obligatorio',
            'description.required' => 'El campo descripción es obligatorio',
            'price.required' => 'El campo precio es obligatorio'
        ]);

        $package = new Package();
        $package->title = $data['title'];
        $package->brief_description = $data['brief_description'];
        $package->description = $data['description'];
        if ($request->file('image')){
            $path = Facades\Storage::disk('public')->put('packages', $request->file('image'));
            $package->image = $path;
        }
        $package->price = $data['price'];
        $package->save();
        return redirect('admin/paquetes')->with('success', 'El paquete se creo correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::findOrFail($id);
        return view('packages.view', compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::findOrFail($id);
        return view('packages.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package = Package::findOrFail($id);
        $data = $request->validate([
            'title' => 'required|unique:packages,id|string|min:4',
            'description' => 'required',
            'price' => 'required',
            'brief_description' => 'required',
        ], [
            'title.required' => 'El campo titulo es obligatorio',
            'title.unique' => 'El titulo para el paquete ya se encuentra registrado',
            'title.min' => 'El campo titulo debe tener como minimo 4 caracteres',
            'brief_description.required' => 'El campo codigo es obligatorio',
            'description.required' => 'El campo descripción es obligatorio',
            'price.required' => 'El campo precio es obligatorio'
        ]);
        if ($request->file('image')){
            $path = Facades\Storage::disk('public')->put('packages', $request->file('image'));
            $package->image = $path;
        }

        $package->title = $data['title'];
        $package->description = $data['description'];
        $package->brief_description = $data['brief_description'];
        $package->price = $data['price'];
        $package->update();

        return redirect('admin/paquetes')->with('success', 'El paquete se actualizo correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Package::findOrFail($id);
        $package->delete();
        return redirect('admin/paquetes')->with('success', 'El paquete se elimino correctamente');
    }
}
