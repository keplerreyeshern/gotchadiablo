<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save();

        $role = new Role();
        $role->name = 'user';
        $role->description = 'User';
        $role->save();

        $user = new User();
        $user->name = 'Kepler Reyes';
        $user->email = 'keplerreyeshern@gmail.com';
        $user->password = Hash::make('GotchaDiablo.2020');
        $user->save();
    }
}
