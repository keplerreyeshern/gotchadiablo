<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/', 'MainController@index')->name('main');
Route::get('/galeria', 'MainController@gallery')->name('galleryF');
Route::get('/contacto', 'MainController@contact')->name('contactF');
Route::get('/paquetes', 'MainController@packages')->name('packagesF');
Route::get('/paquetes/{package}', 'MainController@showPackage')->name('show_package');
Route::get('/reservaciones', 'MainController@reservations')->name('reservationsF')->middleware(['auth', 'verified']);
Route::put('/actualizar/{user}', 'MainController@updateUser')->name('updateUser');
Route::get('/reservacion/crear', 'HomeController@createReservation')->name('createReservation');
Route::post('/reservacion/store', 'MainController@storeReservation')->name('storeReservation');
Route::post('/contacto/store', 'MainController@storeContact')->name('storeContact');

Auth::routes(['verify' => true]);


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'DashboardController@index')->name('dashboard')->middleware(['auth', 'verified','profile']);

Route::resource('/admin/paquetes', 'PackagesController')->middleware(['auth', 'verified', 'profile'])->names([
    'index' => 'packages.index',
    'create' => 'packages.create',
    'store' => 'packages.store',
    'show' => 'packages.show',
    'edit' => 'packages.edit',
    'update' => 'packages.update',
    'destroy' => 'packages.destroy',
])->parameters([
    'paquetes' => 'package'
]);

Route::resource('/admin/imagenes', 'ImagesController')->middleware(['auth', 'verified', 'profile'])->names([
    'index' => 'images.index',
    'create' => 'images.create',
    'store' => 'images.store',
    'show' => 'images.show',
    'edit' => 'images.edit',
    'update' => 'images.update',
    'destroy' => 'images.destroy',
])->parameters([
    'imagenes' => 'image'
]);

Route::resource('/admin/reservaciones', 'ReservationsController')->middleware(['auth', 'verified', 'profile'])->names([
    'index' => 'reservations.index',
    'create' => 'reservations.create',
    'store' => 'reservations.store',
    'show' => 'reservations.show',
    'edit' => 'reservations.edit',
    'update' => 'reservations.update',
    'destroy' => 'reservations.destroy',
])->parameters([
    'reservaciones' => 'reservation'
])->only([
    'index', 'show', 'destroy'
]);
