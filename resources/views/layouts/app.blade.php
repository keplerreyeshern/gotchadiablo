<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Gotcha Diablo">
    <meta property="og:site_name" content="Gotcha-Diablo">
    <meta property="keywords" content="gotcha, go-karts, karts, caretas, balas, protectores, diverción, paquetes, fiestas, cumpleaños">
    <meta property="description" content="Tu mejor alternativa para Gotcha en Toluca">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/custom.js') }}" ></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Chat -->
    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '73b8d3d3e6d0542258cc3952623232420cbcf72b';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
    </script>
    {!! NoCaptcha::renderJs() !!}
</head>
<body>
<!-- header area -->
<header>
    <!-- secondary menu -->
    <nav class="secondary-menu d-none d-md-block">
        <div class="container">
            <!-- secondary menu left link area -->
            <div class="sm-left">
                <strong>Telefono</strong>:&nbsp; <a href="#">7225850178</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <strong>Correo</strong>:&nbsp; <a href="#">atencion@gotchadiablo.com</a>
            </div>
            <!-- secondary menu right link area -->
            <div class="sm-right">
                <!-- social link -->
                <div class="sm-social-link">
                    <a class="h-google" href="{{ route('dashboard') }}" target="_blank">
                        <i class="fa fa-cogs"></i>
                    </a>
                    <a class="h-facebook" href="https://www.facebook.com/gotchadiablo" target="_blank">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <a class="h-twitter" href="#" target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                    @auth
                        <a class="h-google" href=""
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out-alt"></i>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <a class="nav-link dropdown-toggle" href="{{ route('home') }}">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                    @else
                        <a class="h-google " href="{{ route('login') }}">
                            <i class="fa fa-sign-in-alt"></i>
                        </a>
                        @if (Route::has('register'))
                            <a class="h-google" href="{{ route('register') }}">
                                <i class="fa fa-user"></i>
                            </a>
                        @endif
                    @endauth
                </div>
            </div>
        </div>
    </nav>
    <div class="d-none d-md-block" style="min-height: 40px">.</div>
    <!-- primary menu -->
    <nav class="navbar navbar-expand-md navbar-dark">
        <!-- logo area -->
        <a class="navbar-brand" href="{{ route('main') }}">
            <!-- logo image -->
            <img class="img-responsive ml-5" src="{{ asset('images/logo.png') }}" alt="Logo" height="50px"/>
        </a>
        <button class="btn text-white d-md-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li>
                    <a class="" href="{{ route('main') }}">
                        Inicio
                    </a>
                </li>
                <li>
                    <a class="" href="{{ route('reservationsF') }}">
                        Reservaciones
                    </a>
                </li>
                <li>
                    <a class="" href="{{ route('packagesF') }}">
                        Paquetes
                    </a>
                </li>
                <li>
                    <a class="" href="{{ route('galleryF') }}">
                        Galeria
                    </a>
                </li>
                <li>
                    <a class="" href="{{ route('contactF') }}">
                        Contacto
                    </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav><!-- /.container-fluid -->
</header>
<div style="clear: both;"></div>
<!--/ header end -->
<!-- Content -->
<main class="content pt-4">
    @yield('content')
</main>
<!-- Content end -->
<!-- Footer -->
<footer class="pt-5 text-muted text-center text-small">
    <div class="container">
        <!-- social media links -->
        <div class="social">
            <a class="h-facebook" href="#" target="_blank">
                <i class="fab fa-facebook"></i>
            </a>
            <a class="h-twitter" href="#" target="_blank">
                <i class="fab fa-twitter"></i>
            </a>
            <a class="h-google" href="#" target="_blank">
                <i class="fab fa-youtube"></i>
            </a>
            <a class="h-instagram" href="#" target="_blank">
                <i class="fab fa-instagram"></i>
            </a>
            @auth
                <a class="h-google d-md-none" href=""
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out-alt"></i>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                <a class="d-md-none" href="{{ route('home') }}">
                    <span class="caret small">{{ Auth::user()->name }} </span>
                </a>
            @else
                <a class="h-google d-md-none" href="{{ route('login') }}">
                    <i class="fa fa-sign-in-alt"></i>
                </a>
                @if (Route::has('register'))
                    <a class="h-google d-md-none" href="{{ route('register') }}">
                        <i class="fa fa-user"></i>
                    </a>
                @endif
            @endauth
        </div>
        <!-- copy right -->
        <p class="mb-1">&copy; {{ date("Y") }} Gotcha Diablo Todos los derechos reservados.</p>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Privacidad</a></li>
            <li class="list-inline-item"><a href="#">Terminos y Condiciones</a></li>
        </ul>
    </div>
</footer>
<!-- Footer end -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(window).ready(function (){
        var images = $(".image");

        $(images).on("error", function(event) {
            $(event.target).css("display", "none");
        });
    });
</script>
</body>
</html>
