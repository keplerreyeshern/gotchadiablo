@extends('layouts.app')

@section('title', 'Verificar el correo')

@section('content')
<div class="container text-white">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <p class="text-center display-3 d-none d-md-block">{{ __('Verifique su dirección de correo electrónico') }}</p>
            <p class="text-center h1 d-md-none">{{ __('Verifique su dirección de correo electrónico') }}</p>
            @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    <p class="display-1 d-none d-md-block">{{ __('Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.') }}</p>
                    <p class="h1 d-md-none">{{ __('Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.') }}</p>
                </div>
            @endif
            <p class="h1 d-none d-md-block">{{ __('Antes de continuar, revise su correo electrónico para obtener un enlace de verificación.') }}</p>
            <p class="h4 d-md-none">{{ __('Antes de continuar, revise su correo electrónico para obtener un enlace de verificación.') }}</p>
            <p class="h1 d-none d-md-block">{{ __('Si no recibiste el correo electrónico') }}, </p>
            <form class="d-inline d-none d-md-block" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit" class="btn btn-link btn-link-s text-white">{{ __('haga clic aquí para solicitar otro.') }}</button>
            </form>
            <p class="h4 d-md-none">{{ __('Si no recibiste el correo electrónico') }},</p>
            <form class="d-inline d-md-none" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit" class="btn btn-link text-white">{{ __('haga clic aquí para solicitar otro.') }}</button>
            </form>
        </div>
    </div>
</div>
@endsection
