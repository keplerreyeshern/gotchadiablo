@extends('layouts.app')

@section('title', 'Bienvenido a Gotcha Diablo')

@section('content')
    <div class="container text-white">
        <div class="row justify-content-center bg-content">
            <div class="col-md-12">
                <p class="text-center display-1 d-none d-md-block">¡Bienvenido a Gotcha Diablo!</p>
                <p class="text-center h1 d-md-none">¡Bienvenido a Gotcha Diablo!</p>
                <div class="row d-flex align-items-center">
                    <div class="col-12 col-md-7">
                        <p class="display-4 text-center d-none d-md-block">Nuestra Mision:</p>
                        <p class="h2 text-center d-md-none">Nuestra Mision:</p>
                        <p class="text-justify h1 d-none d-md-block">
                            Ser el primer campo de gotcha en otorgar a nuestros clientes un servicio y atencion de calidad para llevar al maximo
                            la experiencia ludica de este deporte, siempre observando las normas de seguridad y reglamentos de juego establecidos
                            par la practica del mismo.
                        </p>
                        <p class="text-justify h4 d-md-none">
                            Ser el primer campo de gotcha en otorgar a nuestros clientes un servicio y atencion de calidad para llevar al maximo
                            la experiencia ludica de este deporte, siempre observando las normas de seguridad y reglamentos de juego establecidos
                            par la practica del mismo.
                        </p>
                    </div>
                    <div class="col-12 col-md-5">
                        <img src="{{ 'images/img-mision.jpeg' }}" class="img-fluid img-thumbnail" alt="Misión">
                    </div>
                </div>
            </div>
        </div>
        <p class="display-1 text-center mt-3 d-none d-md-block">Paquetes</p>
        <p class="h1 text-center mt-3 d-md-none">Paquetes</p>
        <div class="row justify-content-center mt-3">
            @foreach($packages as $package)
                <div class="col-12 col-md-6 col-lg-4">
                    <h1 class="text-center mt-2">{{ $package->title }}</h1>
                    <img src="{{ Storage::url($package->image) }}" class="img-fluid img-thumbnail mt-2 h-50 w-100 image"  alt="{{ $package->image }}">
                    <h3 class="text-center mt-2">{{ $package->brief_description }}</h3>
                    <h3 class="text-center mt-2">$ {{ number_format($package->price, 2, '.', ',') }}</h3>
                    <a href="{{ route('show_package', ['package' => $package->id]) }}" class="btn btn-block btn-outline-danger">
                        <i class="fa fa-eye"></i>
                        ver mas
                    </a>
                </div>
            @endforeach
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <video src="{{ asset('video/ruta.mp4') }}" class="img-fluid" controls>
                        Tu navegador no admite el elemento <code>video</code>.
                    </video>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-2">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d941.7529748044429!2d-99.60565715772506!3d19.23831317672128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cd8b8fb4b01c51%3A0xf18adf1c6157f1d8!2sGotcha%20Diablo!5e0!3m2!1ses-419!2smx!4v1595995836118!5m2!1ses-419!2smx" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
@endsection
