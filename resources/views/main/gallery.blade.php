@extends('layouts.app')

@section( 'title', 'Galeria' )

@section('content')
    <div class="container">
        <!-- our team -->
        <div class="gallery">
            <div class="container">
                <p class="text-white text-center mx-3 display-1 d-none d-md-block">Galeria</p>
                <p class="text-white text-center mx-3 h1 d-md-none">Galeria</p>
                <!-- Team Member's Details -->
                <div class="team-content">
                    <div class="row">
                        <ul class="hoverbox">
                        @foreach($images as $image)
                                <li>
                                    <a href="#">
                                        <img src="{{  Storage::url($image->image) }}" alt="{{  Storage::url($image->image) }}" />
                                        <img src="{{  Storage::url($image->image) }}" alt="{{  Storage::url($image->image) }}" class="preview" />
                                    </a>
                                </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
