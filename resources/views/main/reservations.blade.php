@extends('layouts.app')

@section('title', 'Reservaciones')

@section('content')
    <div class="container text-white mb-5 bg-content">
        <p class="display-4 d-none d-md-block text-center">¡Hola {{ Auth::user()->name }}, Estas son tus reservaciones!</p>
        <p class="h1 d-md-none text-center">¡Hola {{ Auth::user()->name }}, Estas son tus reservaciones!</p>
        <div class="row d-flex justify-content-end">
            <a href="{{ route('createReservation') }}" class="btn btn-sm btn-outline-primary">
                <i class="fa fa-plus"></i>
                Nuevo
            </a>
        </div>
        <div class="table-responsive mt-3">
            <table class="table text-white">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Paquete</th>
                    <th>Fecha</th>
                    <th>Status</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($reservations as $reservation)
                    <tr>
                        <td>{{ $reservation->id }}</td>
                        <td>{{ $reservation->package->title }}</td>
                        <td>{{ $reservation->date }}</td>
                        <td>
                            @if($reservation->status == 1)
                                <span class="text-primary">{{ __('Enviada') }}</span>
                            @elseif($reservation->status == 2)
                                <span class="text-warning">{{ __('Proceso') }}</span>
                            @elseif($reservation->status == 3)
                                <span class="text-success">{{ __('Atendida') }}</span>
                            @elseif($reservation->status == 4)
                                <span class="text-secondary">{{ __('Caducada') }}</span>
                            @elseif($reservation->status == 5)
                                <span class="text-danger">{{ __('Caducada sin Atención') }}</span>
                            @endif
                        </td>
                        <td>
                            @if($reservation->status < 3)
                                <form action="{{ route('reservations.destroy',['reservation' => $reservation->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-danger">
                                        <i class="fa fa-ban"></i>
                                        Cancelar
                                    </button>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
