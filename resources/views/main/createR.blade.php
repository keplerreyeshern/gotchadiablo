@extends('layouts.app')

@section('title', 'Crear Reservación')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-2 bg-content text-white">

                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{ __('Nueva Reservación') }}</h4>
                    <p class="card-category"></p>
                </div>

                <div class="card-body">
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <p style="color: white !important;">{{ $error }}</p>
                        @endforeach
                    @endif
                    <form action="{{ route('storeReservation') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <label for="package">Paquete</label>
                        <select name="package" id="package" class="form-control">
                            <option value="">Seleciona el Paquete</option>
                            @foreach($packages as $package)
                                <option value="{{ $package->id }}">{{ $package->title }}</option>
                            @endforeach
                        </select>
                        <label for="date">Fecha</label>
                        <input type="date" name="date" id="date" class="form-control" placeholder="Fecha" value="{{ old('date') }}">

                        <div class="row mt-3">
                            <div class="col-4"></div>
                            <div class="col-4">
                                <button type="submit" class="btn btn-block btn-outline-primary">
                                    <i class="fa fa-save"></i>
                                    Guardar
                                </button>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </form>
                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-outline-primary">
                        <i class="fa fa-backward"></i>
                        Atras
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
