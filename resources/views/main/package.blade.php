@extends('layouts.app')

@section('title', 'Paquete '. $package->title)

@section('content')
    <div class="container text-white mb-5 bg-content">
        <div class="table-responsive">
            <table class="table text-white">
                <tbody>
                <tr>
                    <th>Nombre</th>
                    <td>{{ $package->title }}</td>
                </tr>
                <tr>
                    <th>Descripción</th>
                    <td>{{ $package->description }}</td>
                </tr>
                <tr>
                    <th>Precio</th>
                    <td>$ {{ number_format($package->price, 2, '.', ',') }}</td>
                </tr>
                <tr>
                    <th>Imagen</th>
                    <td>
                        <img src="{{ Storage::url($package->image) }}" class="img-fluid img-thumbnail w-25 image" alt="{{ $package->image }}">
                    </td>
                </tr>
                </tbody>
            </table>
            <a href="{{ URL::previous() }}" class="btn btn-sm btn-outline-danger">
                <i class="fa fa-backward"></i>
                Atras
            </a>
        </div>
    </div>
@endsection
