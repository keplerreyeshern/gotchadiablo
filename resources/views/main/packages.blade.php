@extends('layouts.app')

@section('title', 'Paquetes')

@section('content')
    <div class="container text-white mb-5 bg-content">
        <div class="table-responsive">
            <table class="table text-white">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($packages as $package)
                    <tr>
                        <td>{{ $package->id }}</td>
                        <td>{{ $package->title }}</td>
                        <td>{{ $package->brief_description }}</td>
                        <td>$ {{ number_format($package->price, 2, '.', ',') }}</td>
                        <td>
                            <a href="{{ route('show_package', ['package' => $package->id]) }}" class="btn btn-sm btn-outline-danger">
                                <i class="fa fa-eye"></i>
                                Ver mas
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
