@extends('layouts.app')

@section( 'title', 'Contacto' )

@section('content')
    <div class="container bg-content text-white pb-5 pt-3" >
        <!-- form content -->
        <div class="form-content ">
            <!-- paragraph -->
            <h3 class="text-center">¿Tienes alguna idea en mente? Escríbenos.</h3>
            @if($errors->any())
                @foreach($errors->all() as $error)
                    <p style="color: white !important;">{{ $error }}</p>
                @endforeach
            @endif
            <form role="form" action="{{ route('storeContact') }}" id="contactForm" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Correo</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Correo" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label for="phone">Telefono</label>
                            <input type="telephone" class="form-control" id="phone" name="phone" placeholder="Telefono" value="{{ old('phone') }}">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <label for="message">Mensaje</label>
                            <textarea class="form-control" id="message" name="message" rows="9" placeholder="Mensaje">{{ old('message') }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Captcha</label>
                    <div class="col-md-6">
                        {!! app('captcha')->display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-lg btn-outline-danger">
                        <i class="fa fa-paper-plane"></i>
                        Enviar Mensaje
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
