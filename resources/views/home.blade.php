@extends('layouts.app')

@section('title', '¡Hola  '. Auth::user()->name .'!')

@section('content')
<div class="container" style="color: white;">
    <div class="row justify-content-center">
        <div class="col-md-10 bg-content">
            <p class="display-4 d-none d-md-block text-center">¡Hola {{ Auth::user()->name }}!</p>
            <p class="h1 d-md-none text-center">¡Hola {{ Auth::user()->name }}!</p>
            <div class="row my-3">
                <div class="col-12 col-md-2">
                    @if(Auth::user()->image)
                        <img src="{{ Storage::url(Auth::user()->image) }}" class="img-fluid rounded-circle" alt="{{ Auth::user()->image }}">
                    @else
                        <img src="{{ asset('images/sinuser.jpg') }}" class="img-fluid rounded-circle" alt="Sin Foto">
                    @endif
                    <!-- Button trigger modal -->
                        <a href="#" class="text-center text-white tooltip-test" title="Tooltip" data-toggle="modal" data-target="#exampleModal">
                            <i class="fa fa-edit"></i>
                            editar
                        </a>
                </div>
                <div class="col-12 col-md-10">
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    @endif
                    <form action="{{ route('updateUser', ['user' => Auth::user()->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="text" name="name" class="form-control" placeholder="Nombre" value="{{ old('name', Auth::user()->name) }}">
                        <input type="email" name="email" class="form-control mt-3" placeholder="Email" value="{{ old('email', Auth::user()->email) }}">
                        <div class="row mt-3">
                            <div class="col-12 col-md-4 offset-md-4">
                                <button type="submit" class="btn btn-block btn-outline-warning">
                                    <i class="fa fa-edit"></i>
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title" id="exampleModalLabel">Editar Avatar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-dark">
                <form action="{{ route('images.update', ['image' => Auth::user()->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="file" class="form-control mt-3" name="image">
            </div>
            <div class="modal-footer bg-dark">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-ban"></i>
                    Cerrar
                </button>
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                    Editar
                </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
