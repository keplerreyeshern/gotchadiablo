@extends('layouts.admin')

@section('title', 'Detalle de la Imagen '. $image->title)
@section('page', 'Imagen '. $image->title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title"> {{ __('Detalle de la Imagen ') . $image->title }}</h4>
                    <p class="card-category"></p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody class="text-dark">
                            <tr>
                                <th>Id</th>
                                <td>{{ $image->id }}</td>
                            </tr>
                            <tr>
                                <th>Nombre</th>
                                <td>{{ $image->title }}</td>
                            </tr>
                            <tr>
                                <th>Descripción</th>
                                <td>{{ $image->description }}</td>
                            </tr>
                            <tr>
                                <th>Imagen</th>
                                <td>
                                    <img src="{{ Storage::url($image->image) }}" class="img-fluid img-thumbnail d-none d-md-block w-25" alt="{{ $image->image }}">
                                    <img src="{{ Storage::url($image->image) }}" class="img-fluid img-thumbnail d-md-none w-100" alt="{{ $image->image }}">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <a href="{{ URL::previous() }}" class="btn btn-sm btn-outline-primary">
                            <i class="fa fa-backward"></i>
                            Atras
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
