@extends('layouts.admin')

@section('title', 'Editar Imagen ' . $image->title)
@section('page', 'Editar Imagen ' . $image->title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{ __('Editar Imagen ') . $image->title }}</h4>
                    <p class="card-category"></p>
                </div>

                <div class="card-body">
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    @endif
                    <form action="{{ route('images.update', ['image' => $image->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="text" name="title" class="form-control" placeholder="Nombre" value="{{ old('title', $image->title) }}">
                        <textarea type="text" name="description" class="form-control mt-3" placeholder="Descripción">{{ old('description', $image->description) }}</textarea>
                        <input type="file" class="form-control mt-3" name="image">
                        <div class="row mt-3">
                            <div class="col-12 col-md-4 offset-md-4">
                                <button type="submit" class="btn btn-block btn-outline-warning">
                                    <i class="fa fa-edit"></i>
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </form>
                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-outline-primary">
                        <i class="fa fa-backward"></i>
                        Atras
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
