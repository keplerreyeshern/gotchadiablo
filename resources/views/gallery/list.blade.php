@extends('layouts.admin')

@section('title', 'Administrar Galeria')
@section('page', 'Lista de Imagenes')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row d-flex justify-content-end">
                <a href="{{ route('images.create') }}" class="btn btn-sm btn-outline-primary">
                    <i class="fa fa-plus"></i>
                    Nuevo
                </a>
            </div>
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Lista</h4>
                    <p class="card-category">Imagenes</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-dark">
                            <tr class="text-center">
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Imagen</th>
                                <th colspan="3" class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($images as $image)
                                <tr class="text-center">
                                    <td>{{ $image->id }}</td>
                                    <td>{{ $image->title }}</td>
                                    <td>{{ $image->description }}</td>
                                    <td width="50px">
                                        <img src="{{ Storage::url($image->image) }}" class="img-fluid img-thumbnail" alt="{{ $image->image }}">
                                    </td>
                                    <td>
                                        <a href="{{ route('images.show', ['image' => $image->id ]) }}" class="btn btn-outline-info btn-sm">
                                            <i class="fa fa-eye"></i>
                                            Ver más
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('images.edit', ['image' => $image->id ]) }}" class="btn btn-outline-warning btn-sm">
                                            <i class="fa fa-edit"></i>
                                            Editar
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ route('images.destroy',['image' => $image->id]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                                <i class="fa fa-trash-alt"></i>
                                                Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="navigation">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
