@extends('layouts.admin')

@section('title', 'Administración de Gotcha Diablo')
@section('page', 'Tablero')

@section('content')

    <div class="row">
        <div class="col-12 col-md-6 mt-5">
            <p class="h1 text-center" >{{ __('Paquetes: ') . $countPackages }}</p>
            <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{ $countPackages }}%" aria-valuenow="{{ $countPackages }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="col-12 col-md-6 mt-5">
            <p class="h1 text-center" >{{ __('Reservaciones: ') . $countReservations }}</p>
            <div class="progress">
                <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{ $countReservations }}%" aria-valuenow="{{ $countReservations }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="col-12 col-md-6 mt-5">
            <p class="h1 text-center" >{{ __('Imagenes: ') . $countImages }}</p>
            <div class="progress">
                <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: {{ $countImages }}%" aria-valuenow="{{ $countImages }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="col-12 col-md-6 mt-5">
            <p class="h1 text-center" >{{ __('Usuarios: ') . $countUsers }}</p>
            <div class="progress">
                <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: {{ $countUsers }}%" aria-valuenow="{{ $countUsers }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>
@endsection
