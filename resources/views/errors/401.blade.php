@extends('errors::illustrated-layout')

@section('code', '401')
@section('title', __('Sin Autorización'))

@section('image')
    <div style="background-image: url('/svg/401.svg');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __('Disculpa, pero no tienes acceso a esta pagina.'))
