@extends('errors::illustrated-layout')

@section('code', '404')
@section('title', __('Pagina no Encontrada'))

@section('image')
    <div style="background-image: url('/svg/401.svg');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __('Upps algo salio mal, la pagina que estas buscando no existe'))
