@extends('layouts.admin')

@section('title', 'Administrar Reservaciones')
@section('page', 'Lista de Reservaciones')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row d-flex justify-content-end">
{{--                <a href="{{ route('images.create') }}" class="btn btn-sm btn-outline-primary">--}}
{{--                    <i class="fa fa-plus"></i>--}}
{{--                    Nuevo--}}
{{--                </a>--}}
            </div>
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Lista</h4>
                    <p class="card-category">Reservaciones</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-dark">
                            <tr class="text-center">
                                <th>Id</th>
                                <th>Usuario</th>
                                <th>Paquete</th>
                                <th>Fecha</th>
                                <th>Status</th>
                                <th colspan="3" class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reservations as $reservation)
                                <tr class="text-center">
                                    <td>{{ $reservation->id }}</td>
                                    <td>{{ $reservation->user->name }}</td>
                                    <td>{{ $reservation->package->title }}</td>
                                    <td>{{ $reservation->date }}</td>
                                    <td>
                                        @if($reservation->status == 1)
                                            <span class="text-primary">{{ __('Enviada') }}</span>
                                        @elseif($reservation->status == 2)
                                            <span class="text-warning">{{ __('Proceso') }}</span>
                                        @elseif($reservation->status == 3)
                                            <span class="text-success">{{ __('Atendida') }}</span>
                                        @elseif($reservation->status == 4)
                                            <span class="text-secondary">{{ __('Caducada') }}</span>
                                        @elseif($reservation->status == 5)
                                            <span class="text-danger">{{ __('Caducada sin Atención') }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('reservations.show', ['reservation' => $reservation->id ]) }}" class="btn btn-outline-info btn-sm">
                                            <i class="fa fa-eye"></i>
                                            Ver más
                                        </a>
                                    </td>
{{--                                    <td>--}}
{{--                                        <a href="{{ route('images.edit', ['image' => $image->id ]) }}" class="btn btn-outline-warning btn-sm">--}}
{{--                                            <i class="fa fa-edit"></i>--}}
{{--                                            Editar--}}
{{--                                        </a>--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <form action="{{ route('images.destroy',['image' => $image->id]) }}" method="POST">--}}
{{--                                            @csrf--}}
{{--                                            @method('DELETE')--}}
{{--                                            <button type="submit" class="btn btn-sm btn-outline-danger">--}}
{{--                                                <i class="fa fa-trash-alt"></i>--}}
{{--                                                Eliminar--}}
{{--                                            </button>--}}
{{--                                        </form>--}}
{{--                                    </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="navigation">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
