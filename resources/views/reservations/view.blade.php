@extends('layouts.admin')

@section('title', 'Detalle de la reservación '. $reservation->user->name)
@section('page', 'Reservación '. $reservation->user->name)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title"> {{ __('Detalle de la reservación ') . $reservation->user->name }}</h4>
                    <p class="card-category"></p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody class="text-dark">
                            <tr>
                                <th>Id</th>
                                <td>{{ $reservation->id }}</td>
                            </tr>
                            <tr>
                                <th>Usuario</th>
                                <td>{{ $reservation->user->name }}</td>
                            </tr>
                            <tr>
                                <th>Paquete</th>
                                <td>{{ $reservation->package->title }}</td>
                            </tr>
                            <tr>
                                <th>Fecha</th>
                                <td>{{ $reservation->date }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($reservation->status == 1)
                                        <span class="text-primary">{{ __('Enviada') }}</span>
                                    @elseif($reservation->status == 2)
                                        <span class="text-warning">{{ __('Proceso') }}</span>
                                    @elseif($reservation->status == 3)
                                        <span class="text-success">{{ __('Atendida') }}</span>
                                    @elseif($reservation->status == 4)
                                        <span class="text-secondary">{{ __('Caducada') }}</span>
                                    @elseif($reservation->status == 5)
                                        <span class="text-danger">{{ __('Caducada sin Atención') }}</span>
                                    @endif
                                </td>
                            </tr>
                            @if($reservation->status < 3)
                                <tr>
                                    <th>Cambiar Status</th>
                                    <td>
                                        <select name="status" id="status" class="form-control">
                                            <option value="0">Seleccionar Status</option>
                                            <option value="2">Proceso</option>
                                            <option value="3">Atendida</option>
                                        </select>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <hr>
                        <a href="{{ URL::previous() }}" class="btn btn-sm btn-outline-primary">
                            <i class="fa fa-backward"></i>
                            Atras
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
