@extends('layouts.admin')

@section('title', 'Editar Paquete ' . $package->title)
@section('page', 'Editar Paquete ' . $package->title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{ __('Editar Paquete ') . $package->title }}</h4>
                    <p class="card-category"></p>
                </div>

                <div class="card-body">
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    @endif
                    <form action="{{ route('packages.update', ['package' => $package->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="text" name="title" class="form-control" placeholder="Nombre" value="{{ old('title', $package->title) }}">
                        <textarea type="text" name="brief_description" class="form-control mt-3" placeholder="Breve Descripción">{{ old('brief_description', $package->brief_description) }}</textarea>
                        <textarea type="text" name="description" class="form-control mt-3" placeholder="Descripción">{{ old('description', $package->description) }}</textarea>
                        <input type="number" name="price" step="any" class="form-control mt-3" placeholder="Precio" value="{{ old('price', $package->price) }}">
                        <input type="file" class="form-control mt-3" name="image">
                        <div class="row mt-3">
                            <div class="col-12 col-md-4 offset-md-4">
                                <button type="submit" class="btn btn-block btn-outline-warning">
                                    <i class="fa fa-edit"></i>
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </form>
                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-outline-primary">
                        <i class="fa fa-backward"></i>
                        Atras
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
