@extends('layouts.admin')

@section('title', 'Detalle del Paquete '. $package->title)
@section('page', 'Paquete '. $package->title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title"> {{ __('Detalle del Paquete') . $package->title }}</h4>
                    <p class="card-category"></p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody class="text-dark">
                            <tr>
                                <th>Id</th>
                                <td>{{ $package->id }}</td>
                            </tr>
                            <tr>
                                <th>Nombre</th>
                                <td>{{ $package->title }}</td>
                            </tr>
                            <tr>
                                <th>Descripción</th>
                                <td>{{ $package->description }}</td>
                            </tr>
                            <tr>
                                <th>Precio</th>
                                <td>$ {{ number_format($package->price, 2, '.', ',') }}</td>
                            </tr>
                            <tr>
                                <th>Imagen</th>
                                <td>
                                    <img src="{{ Storage::url($package->image) }}" class="img-fluid img-thumbnail d-none d-md-block w-25" alt="{{ $package->image }}">
                                    <img src="{{ Storage::url($package->image) }}" class="img-fluid img-thumbnail d-md-none w-100" alt="{{ $package->image }}">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <a href="{{ URL::previous() }}" class="btn btn-sm btn-outline-primary">
                            <i class="fa fa-backward"></i>
                            Atras
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
