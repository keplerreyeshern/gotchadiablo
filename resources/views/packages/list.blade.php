@extends('layouts.admin')

@section('title', 'Administrar Paquetes')
@section('page', 'Lista de Paquetes')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row d-flex justify-content-end">
                <a href="{{ route('packages.create') }}" class="btn btn-sm btn-outline-primary">
                    <i class="fa fa-plus"></i>
                    Nuevo
                </a>
            </div>
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Lista</h4>
                    <p class="card-category">Paquetes</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-dark">
                            <tr class="text-center">
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Precio</th>
                                <th>Imagen</th>
                                <th colspan="3" class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($packages as $package)
                                    <tr class="text-center">
                                        <td>{{ $package->id }}</td>
                                        <td>{{ $package->title }}</td>
                                        <td>{{ $package->brief_description }}</td>
                                        <td>$ {{ number_format($package->price, 2, '.', ',') }}</td>
                                        <td width="50px">
                                            <img src="{{ Storage::url($package->image) }}" class="img-fluid img-thumbnail image" alt="{{ $package->image }}">
                                        </td>
                                        <td>
                                            <a href="{{ route('packages.show', ['package' => $package->id ]) }}" class="btn btn-outline-info btn-sm">
                                                <i class="fa fa-eye"></i>
                                                Ver más
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('packages.edit', ['package' => $package->id ]) }}" class="btn btn-outline-warning btn-sm">
                                                <i class="fa fa-edit"></i>
                                                Editar
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{ route('packages.destroy',['package' => $package->id]) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-outline-danger">
                                                    <i class="fa fa-trash-alt"></i>
                                                    Eliminar
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="navigation">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
