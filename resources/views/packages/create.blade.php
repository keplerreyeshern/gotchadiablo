@extends('layouts.admin')

@section('title', 'Crear nuevo Paquete')
@section('page', 'Crear nuevo Paquete')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{ __('Nuevo Paquete') }}</h4>
                    <p class="card-category"></p>
                </div>

                <div class="card-body">
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    @endif
                    <form action="{{ route('packages.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="text" name="title" class="form-control" placeholder="Titulo" value="{{ old('title') }}">
                        <textarea type="text" name="brief_description" class="form-control mt-3" placeholder="Breve Descripción">{{ old('brief_description') }}</textarea>
                        <textarea type="text" name="description" class="form-control mt-3" placeholder="Descripción">{{ old('description') }}</textarea>
                        <input type="number" name="price" step="any" class="form-control mt-3" placeholder="Precio" value="{{ old('price') }}">
                        <input type="file" class="form-control mt-3" name="image">
                        <div class="row">
                            <div class="col-4"></div>
                            <div class="col-4">
                                <button type="submit" class="btn btn-block btn-outline-primary">
                                    <i class="fa fa-save"></i>
                                    Guardar
                                </button>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </form>
                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-outline-primary">
                        <i class="fa fa-backward"></i>
                        Atras
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
