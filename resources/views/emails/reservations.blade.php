<!DOCTYPE html>
<html lang="es">
<head>
    <title> Reservación Realizada por {{ Auth::user()->name }}</title>
</head>
<body>
    <p>Se a creado una resevación a nombre de {{ Auth::user()->name }}</p>
    <p>Con el paquete: {{ $msg['package'] }}</p>
    <p>para la fecha: {{ $msg['date'] }}</p>
    <p><strong>Favor de Revisar y dar un seguimiento a la reservación</strong></p>
</body>
</html>
