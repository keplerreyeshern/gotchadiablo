<!DOCTYPE html>
<html lang="es">
<head>
    <title> Mensaje Enviado por {{ $msg['name'] }}</title>
</head>
<body>
<p>Se Recibio un mensaje de <strong>{{ $msg['name'] }}</strong></p>
<p><strong>Correo: </strong> {{ $msg['email'] }}</p>
<p><strong>Telefono: </strong> {{ $msg['phone'] }}</p>
<p><strong>Mensaje: </strong> {{ $msg['message'] }}</p>
</body>
</html>

